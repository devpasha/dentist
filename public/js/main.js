var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}
var doc = document.getElementsByClassName("doc");
var j;

for (j = 0; j < doc.length; j++) {
  doc[j].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  }
}
function myFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("serv");
    filter = input.value.toUpperCase();
    ul = document.getElementById("serv-select");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        span = li[i].getElementsByTagName("span")[0];
        if (span.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";

        }
    }
}
function myFunctionDocs() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("doc-choice");
    filter = input.value.toUpperCase();
    ul = document.getElementById("doc-select");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        span = li[i].getElementsByTagName("span")[0];
        if (span.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";

        }
    }
}
$("#serv").click(function(){
  $("#serv-select").css("display", "block");
});

var acc = document.getElementsByClassName("services-choice");
var i;
for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        var serv = this.innerHTML;
        $("#serv").val(serv);
        $("#serv-select").css("display", "none");
    }
}

$("#doc-choice").click(function(){
  $("#doc-select").css("display", "block");
});

var doc = document.getElementsByClassName("doc-choices");
var j;
for (j = 0; j < doc.length; j++) {
    doc[j].onclick = function(){
        var docc = this.innerHTML;
        $("#doc-choice").val(docc);
        $("#doc-select").css("display", "none");
    }
}

$(document).ready(function(){
            $("#enroll").click(function(){
                $("#orderModal").toggle();
            });
            $("#schedule_page td").click(function(){
                $("#orderModal").toggle();
            });
            $("#schedule_page td.red_schedule").click(function(){
                $("#orderModal").toggle();
            });
            $(".enroll-doc").click(function(){
                $("#orderModal").toggle();
            });
             $("#enroll-serv").click(function(){
                $("#orderModal").toggle();
            });
            
            var modal = document.getElementById('orderModal');
                window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
                }
        });
    //header
var page = $("#page").text();
if(page === "main"){
    $(".menu .menu-item:nth-child(1)").addClass("active");
} else if(page === "staff"){
    $(".menu .menu-item:nth-child(2)").addClass("active");
} else if(page === "services"){
    $(".menu .menu-item:nth-child(3)").addClass("active");
} else if(page === "price"){
    $(".menu .menu-item:nth-child(4)").addClass("active");
} else if(page === "contacts"){
    $(".menu .menu-item:nth-child(5)").addClass("active");
}