function initMap() {
	var center = {lat:50.445797, lng: 30.518708}
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 17,
		center: center,
		mapTypeControl: true,
		scrollwheel: false,
		zoomControl: true,
		fullscreenControl: true,
		mapTypeId: 'roadmap'
	});
	var contentString = '<div id="content">'+
	'<div id="siteNotice">'+
	'</div>'+
	'<h3 id="firstHeading" class="firstHeading">Стоматология</h3>'+
	'<div id="bodyContent">'+
	'<p>ул. Пушкинская, 11А, Киев</p>'+
	'</div>'+
	'</div>';
	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});
	var marker = new google.maps.Marker({
		position: center,
		map: map
	});
	marker.addListener('click', function() {
		infowindow.open(map, marker);
	});
}