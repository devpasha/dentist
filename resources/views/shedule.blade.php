@extends('body.site')
@section('content')
	<div class="content">
		<section id="schedule-page">
			<div class="fixed">
				<h1>Расписание врачей</h1>
				<form action="/shedule" method="post" id="sched_date">
					{{csrf_field()}}
					<input type="date" name="date">
					<input type="submit" value="Показать" class="">
				</form>
				<table id="schedule_page">
					<tr>
						<th>Врач</th>
						<th>09:00 - 10:00</th>
						<th>10:00 - 11:00</th>
						<th>11:00 - 12:00</th>
						<th>12:00 - 13:00</th>
						<th>13:00 - 14:00</th>
						<th>14:00 - 15:00</th>
						<th>15:00 - 16:00</th>
						<th>16:00 - 17:00</th>
						<th>17:00 - 18:00</th>
					</tr>
					@foreach ($b_doctors as $doctor)
						<tr>
							<td>{{$doctor->name}}, <br>{{$doctor->specialization}}</td>
							{{--День!!!!! 22.02...--}}
							@for($i=0; $i<9; $i++)
								@if($b_doct_time_arr[$doctor->name][$i]['time'] != '19:00')
										@if($b_doct_time_arr[$doctor->name][$i]['status']==1)
											<td class="red_schedule">{{$b_doct_time_arr[$doctor->name][$i]['name']}}</td>
										@else
											<td class="red_schedule">Ожидает</td>
										@endif
								@else
										<td>
{{--											{{$i}}
											<br>
											{{$b_doct_time_arr[$doctor->name][$i]['name']}}
											<br>
											{{$b_doct_time_arr[$doctor->name][$i]['time']}}--}}
										</td>
								@endif
							@endfor
						</tr>
					@endforeach
				</table>
			</div>
		</section>
		<div id="page">staff</div>
	</div>
	</div>
	<div id="orderModal">
		<div id="modal-content">
			<div class="request-form">
				<form action="{{route('pageMain')}}" method="post">
					{{csrf_field()}}
					<h3>Заказать услугу</h3>
					{{--<input type="text" placeholder="Ваше имя" name="name" required><br>--}}
					<input type="text" placeholder="Ваше имя" name="name"><br>
					{{--<input type="text" placeholder="Номер телефона" name="phone" required><br>--}}
					<input type="text" placeholder="Номер телефона" name="phone"><br>
					<input type="time" name="cron" value="09:00" min="09:00" max="17:00"><br>
					<input type="date" name="day" placeholder="Выберите дату"><br>
					<input type="text" id="serv" name="service" onkeyup="myFunction()" placeholder="Вид услуги" >
					<ul id="serv-select">
						<li><span class="services-choice">Общие виды работ</span></li>
						<li><span class="services-choice">Консультация</span></li>
						<li><span class="services-choice">Оказание скорой помощи</span></li>
						<li><span class="services-choice">Рентген</span></li>
					</ul>
					<input type="text" name="doctor" id="doc-choice" onkeyup="myFunctionDocs()" placeholder="Выберите врача" >
					<ul id="doc-select">
						<li><span class="doc-choices">Василий Петрович</span></li>
						<li><span class="doc-choices">Александр Юриевич</span></li>
						<li><span class="doc-choices">Галина Семеновна</span></li>
						<li><span class="doc-choices">Валентина Сергеевна</span></li>
						<li><span class="doc-choices">Мария Анатолиевна</span></li>
						<li><span class="doc-choices">Виктория Степановна</span></li>
					</ul>
					<ul>
						<textarea name="comment"></textarea>
					</ul>
					<input type="submit" value="Записаться" class="enroll-doc">
				</form>
				{{--					<form action="{{route('pageMain')}}" method="post">
                                        {{csrf_field()}}
                                        <h3>Заказать услугу</h3>
                                        <input type="text" placeholder="Ваше имя" name="name" required><br>
                                        <input type="text" placeholder="Номер телефона" name="phone" required><br>
                                        <input type="time" name="cron" value="08:00" min="09:00" max="18:00"><br>
                                        <input type="date" name="day" placeholder="Выберите дату"><br>
                                        <input type="text" id="serv" name="service" onkeyup="myFunction()" placeholder="Вид услуги" >
                                        <ul id="serv-select">
                                            <li><span class="services-choice">Общие виды работ</span></li>
                                            <li><span class="services-choice">Консультация</span></li>
                                            <li><span class="services-choice">Оказание скорой помощи</span></li>
                                            <li><span class="services-choice">Рентген</span></li>
                                        </ul>
                                        <input type="text" name="doctor" id="doc-choice" onkeyup="myFunctionDocs()" placeholder="Выберите врача" >
                                        <ul id="doc-select">
                                            <li><span class="doc-choices">Василий Петрович</span></li>
                                            <li><span class="doc-choices">Александр Юриевич</span></li>
                                            <li><span class="doc-choices">Галина Семеновна</span></li>
                                            <li><span class="doc-choices">Мария Анатолиевна</span></li>
                                            <li><span class="doc-choices">Виктория Степановна</span></li>
                                            <li><span class="doc-choices">Валентина Сергеевна</span></li>
                                        </ul>
                                        <ul>
                                            <textarea name="comment"></textarea>
                                        </ul>
                                        <input type="submit" value="Записаться" class="enroll-doc">
                                    </form>--}}
			</div>
		</div>
	</div>
@endsection

