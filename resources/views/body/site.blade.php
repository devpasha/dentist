<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title>Стоматологическая клиника</title>
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	{{--<link rel="stylesheet" href="css/style.css">--}}
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	{{--<link rel="shortcut icon" href="img/logo.png">--}}
	<link rel="shortcut icon" href="{{asset("img/logo.png")}}">
</head>
<body>
<div id="site">
	<header>
		<div class="header-top">
			<div class="fixed">
				<div class="contact">
					<div class="phone"><a href="tel:0964157004"><i class="fa fa-phone" aria-hidden="true"></i>050 111 11 11</a></div>
					<div class="phone"><i class="fa fa-clock-o" aria-hidden="true"></i>Пн-Сб 09.00-20.00</div>
					<div class="mail"><a href="stomatology@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i>stomatology@gmail.com</a></div>
					<div class="mail"><a href="{{route('registration')}}">Регистрация</a>/<a href="{{route('login')}}">Войти</a></div>
				</div>
			</div>
		</div>
		<div class="header-bottom">
			<div class="fixed">
				<div class="logo">
					<img src="{{asset('img/logo.png')}}" alt="">
				</div>
				<div class="menu">
					<div class="menu-item"><a href="http://dentist">Главная</a></div>
							{{--<div class="menu-item"><a href="http://dentist/staff">Персонал</a></div>--}}
					<div class="menu-item"><a href="{{route('pageStaff')}}">Персонал</a></div>
							{{--<div class="menu-item"><a href="http://dentist/servises">Услуги</a></div>--}}
					<div class="menu-item"><a href="{{route('pageServises')}}">Услуги</a></div>
							{{--<div class="menu-item"><a href="http://dentist/prices">Цены</a></div>--}}
					<div class="menu-item"><a href="{{route('pagePrices')}}">Цены</a></div>
							{{--<div class="menu-item"><a href="http://dentist/contacts">Контакты</a></div>--}}
					<div class="menu-item"><a href="{{route('pageContacts')}}">Контакты</a></div>
				</div>
			</div>
		</div>
	</header>
{{--<body>--}}
		{{-- контент запрошенной страницы --}}
		@yield('content')
		{{-- контент запрошенной страницы --}}
{{--</body>--}}
<footer>
	<div class="fixed">
		<div class="footer-coloumn">
			<ul>
				<li><a href="http://dentist">Главная</a></li>
				<li><a href="http://dentist/staff">Персонал</a></li>
				<li><a href="http://dentist/servises">Услуги</a></li>
				<li><a href="http://dentist/prices">Цены</a></li>
				<li><a href="http://dentist/contacts">Контакты</a></li>
			</ul>
		</div>
		<div class="footer-coloumn">
			<ul>
				<li><a href="">Чистка зубов</a></li>
				<li><a href="">Лечение кариеса</a></li>
				<li><a href="">Отбеливание зубов</a></li>
				<li><a href="">Реставрация зубов</a></li>
				<li><a href="">Лечение корневых каналов</a></li>
				<li><a href="">Лечение пародонтита</a></li>
			</ul>
		</div>
		<div class="footer-coloumn">
			<ul>
				<li><a href="">Удаление зубов</a></li>
				<li><a href="">Имплантация</a></li>
				<li><a href="">Брекеты</a></li>
				<li><a href="">Каппы</a></li>
				<li><a href="">Выравнивание зубов</a></li>
			</ul>
		</div>
		<div class="footer-coloumn">
			<ul>
				<li><a href="">Виниры</a></li>
				<li><a href="">Люминиры</a></li>
				<li><a href="">Коронки металлокерамические</a></li>
				<li><a href="">Коронки безметалловые</a></li>
				<li><a href="">Съемные протезы бюгельные</a></li>
				<li><a href="">Нейлоновые протезы</a></li>
				<li><a href="">Пластмассовые протезы</a></li>
			</ul>
		</div>
		<div class="footer-coloumn">
			<ul>
				<li><a href="tel:0964157004"><i class="fa fa-phone" aria-hidden="true"></i>050 111 11 11</a></li><br>
				<li><i class="fa fa-clock-o" aria-hidden="true"></i>Пн-Сб 09.00-20.00</li><br>
				<li><a href="mailto:stomatology@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i>stomatology@gmail.com</a></li>
			</ul>
		</div>
	</div>
	</footer>
	<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('js/main.js')}}"></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDI-EyYe0E1ZPA9IpCTUbP2137VDAcHJGY&callback=initMap"></script>
	<script src="{{asset('js/map.js')}}"></script>
</div>
</body>
</html>