-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 22 2018 г., 18:43
-- Версия сервера: 5.7.16
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dentist`
--

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `service` varchar(255) NOT NULL,
  `doctor` varchar(255) NOT NULL,
  `day` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id`, `name`, `phone`, `service`, `doctor`, `day`, `time`, `comment`, `status`) VALUES
(1, 'user', '777778', 'Общие виды работ', 'Василий Петрович', '2018-02-01', '09:00', 'Перезвоните, пожалуйста в 15 часов', '1'),
(2, 'Олег', '093-555-555-55', 'Общие виды работ', 'Александр Юриевич', '2018-02-01', '10:00', 'Нужна консультация', '1'),
(3, 'Мария', '(050)44-44-444', 'Оказание скорой помощи', 'Василий Петрович', '2018-02-01', '11:00', 'У вас в клинике есть детская комната?', '0'),
(4, 'Паша', '0675896317', 'Рентген', 'Галина Семеновна', '2018-02-01', '12:00', 'страрые снимки брать?', '1'),
(5, 'Сергей', '0931112233', 'Общие виды работ', 'Виктория Степановна', '2018-02-01', '13:00', 'пломба', '1'),
(6, 'Анна', '0991100271', 'Общие виды работ', 'Валентина Сергеевна', '2018-02-01', '14:00', 'проблема с отбелеванием', '0'),
(7, 'Olga', '80445678921', 'Консультация', 'Василий Петрович', '2018-02-01', '15:00', 'По поводу коронок', '0'),
(8, 'Виктор', '0671231231', 'Консультация', 'Василий Петрович', '2018-02-01', '16:00', 'Болит зуб!', '1'),
(9, 'Лилия', '0501234567', 'Консультация', 'Мария Анатолиевна', '2018-02-01', '14:00', NULL, '0');

-- --------------------------------------------------------

--
-- Структура таблицы `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `specialization` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `doctors`
--

INSERT INTO `doctors` (`id`, `name`, `specialization`, `foto`) VALUES
(1, 'Василий Петрович', 'врач-ортодонт', '/img/testdoctor.jpg'),
(2, 'Мария Анатолиевна', 'врач-хирург', '/img/testdoctor.jpg'),
(3, 'Александр Юриевич', 'врач-протезист', '/img/testdoctor.jpg'),
(4, 'Галина Семеновна', 'врач-дантист', '/img/testdoctor.jpg'),
(5, 'Виктория Степановна', 'врач-дантист', '/img/testdoctor.jpg'),
(6, 'Валентина Сергеевна', 'врач-протезист', '/img/testdoctor.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2018_02_01_125340_add_status_to_clients', 2),
(6, '2018_02_01_134506_change_status_to_clients', 3),
(7, '2018_02_05_105506_changeStatusField', 4),
(8, '2018_02_22_081456_add_nullable_to_users_table', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` varchar(255) CHARACTER SET utf8 NOT NULL,
  `isAdmin` int(10) NOT NULL DEFAULT '0',
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `isAdmin`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, '$2y$10$8xZGmwWc992TrFSBnyZ8r.g5chaOrEO.2kohUaIcwEnzPZk/2dMri', NULL, '2018-01-24 12:13:28', '2018-01-24 12:13:28'),
(5, 'user', 0, '$2y$10$8xZGmwWc992TrFSBnyZ8r.g5chaOrEO.2kohUaIcwEnzPZk/2dMri', NULL, '2018-01-25 10:34:42', '2018-01-25 10:34:42'),
(6, 'Паша', 0, '$2y$10$8xZGmwWc992TrFSBnyZ8r.g5chaOrEO.2kohUaIcwEnzPZk/2dMri', NULL, '2018-02-01 12:03:53', '2018-02-01 12:03:53'),
(7, 'Olga', 0, '$2y$10$8xZGmwWc992TrFSBnyZ8r.g5chaOrEO.2kohUaIcwEnzPZk/2dMri', NULL, NULL, NULL),
(8, 'Виктор', 0, '$2y$10$8xZGmwWc992TrFSBnyZ8r.g5chaOrEO.2kohUaIcwEnzPZk/2dMri', NULL, NULL, NULL),
(11, 'Лилия', 0, '$2y$10$wd5Yo1DmLeqi90li8wuvV.CqdjdZbALt5Qzj1YpmoXG.6nTF8WbPy', NULL, NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`login`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
