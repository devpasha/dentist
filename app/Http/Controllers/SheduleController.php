<?php

namespace App\Http\Controllers;

use App\Client;
use App\Doctor;
use Illuminate\Http\Request;
use Whoops\Exception\ErrorException;

class SheduleController extends Controller
{
    public function shedule(){
        //SELECT d.name as name, c.day as day, c.time as time FROM doctors d LEFT JOIN clients c ON d.name = c.doctor
        $doctors = Doctor::select('name', 'specialization')->get();

        //первой может быть  таблица Доктор и Клиент - без разницы???!?!?!?! тут 6 полей, а выодиться 5: первый name затирается вторым name - как решиь??!?!?!
        $doctors_time = Doctor::select('doctors.name', 'doctors.specialization', 'clients.doctor', 'clients.time', 'clients.name', 'clients.status')->leftJoin('clients', 'doctors.name', '=', 'clients.doctor')->get();
//        $doctors_time = Client::select('doctor', 'time')->leftJoin('doctors', 'clients.doctor', '=', 'doctors.name')->get();
//        echo "<pre>";
        $doctors_time_arr2 = $doctors_time->groupBy('doctor')->toArray();
        $doctors_time_arr3 = [];

        foreach ($doctors as $item){
//            try { // почему не работате ?!7!?!7!7!7!7!
                while (count($doctors_time_arr2[$item->name]) < 9) {
                    array_push($doctors_time_arr2[$item->name], ([
                        'name' => 'noname',
                        'specialization' => 'specialization',
                        'doctor' => 'nodoctor',
                        'time' => '19:00',
                        'status' => '2'])
                    );
                }
//            }
/*            catch (ErrorException $e){
                $doctors_time_arr2[] = $item->name;
                while (count($doctors_time_arr2[$item->name]) < 9) {
                    array_push($doctors_time_arr2[$item->name], ([
                        'name' => 'noname',
                        'specialization' => 'specialization',
                        'doctor' => 'nodoctor',
                        'time' => '19:00',
                        'status' => '2'])
                    );
                }
            }*/
                    // проверяем 1-цу
                    if ($doctors_time_arr2[$item->name][0]['time'] == '09:00') {
                        $doctors_time_arr3[$item->name][0] = $doctors_time_arr2[$item->name][0];
                    } else {
                        $doctors_time_arr3[$item->name][0] = [
                            'name' => 'noname',
                            'specialization' => 'specialization',
                            'doctor' => 'nodoctor',
                            'time' => '19:00',
                            'status' => '2'
                        ];
                    }
                    // проверяем 2-ку
                    if ($doctors_time_arr2[$item->name][0]['time'] == '10:00') {
                        $doctors_time_arr3[$item->name][1] = $doctors_time_arr2[$item->name][0];
                    } elseif ($doctors_time_arr2[$item->name][1]['time'] == '10:00') {
                        $doctors_time_arr3[$item->name][1] = $doctors_time_arr2[$item->name][1];
                    } else {
                        $doctors_time_arr3[$item->name][1] = [
                            'name' => 'noname',
                            'specialization' => 'specialization',
                            'doctor' => 'nodoctor',
                            'time' => '19:00',
                            'status' => '2'
                        ];
                    }
                    // проверяем 3-ку
                    if ($doctors_time_arr2[$item->name][0]['time'] == '11:00') {
                        $doctors_time_arr3[$item->name][2] = $doctors_time_arr2[$item->name][0];
                    } elseif ($doctors_time_arr2[$item->name][1]['time'] == '11:00') {
                        $doctors_time_arr3[$item->name][2] = $doctors_time_arr2[$item->name][1];
                    } elseif ($doctors_time_arr2[$item->name][2]['time'] == '11:00') {
                        $doctors_time_arr3[$item->name][2] = $doctors_time_arr2[$item->name][2];
                    } else {
                        $doctors_time_arr3[$item->name][2] = [
                            'name' => 'noname',
                            'specialization' => 'specialization',
                            'doctor' => 'nodoctor',
                            'time' => '19:00',
                            'status' => '2'
                        ];
                    }
                    // проверяем 4-ку
                    if ($doctors_time_arr2[$item->name][0]['time'] == '12:00') {
                        $doctors_time_arr3[$item->name][3] = $doctors_time_arr2[$item->name][0];
                    } elseif ($doctors_time_arr2[$item->name][1]['time'] == '12:00') {
                        $doctors_time_arr3[$item->name][3] = $doctors_time_arr2[$item->name][1];
                    } elseif ($doctors_time_arr2[$item->name][2]['time'] == '12:00') {
                        $doctors_time_arr3[$item->name][3] = $doctors_time_arr2[$item->name][2];
                    } elseif ($doctors_time_arr2[$item->name][3]['time'] == '12:00') {
                        $doctors_time_arr3[$item->name][3] = $doctors_time_arr2[$item->name][3];
                    } else {
                        $doctors_time_arr3[$item->name][3] = [
                            'name' => 'noname',
                            'specialization' => 'specialization',
                            'doctor' => 'nodoctor',
                            'time' => '19:00',
                            'status' => '2'
                        ];
                    }
                    // проверяем 5-ку
                    if ($doctors_time_arr2[$item->name][0]['time'] == '13:00') {
                        $doctors_time_arr3[$item->name][4] = $doctors_time_arr2[$item->name][0];
                    } elseif ($doctors_time_arr2[$item->name][1]['time'] == '13:00') {
                        $doctors_time_arr3[$item->name][4] = $doctors_time_arr2[$item->name][1];
                    } elseif ($doctors_time_arr2[$item->name][2]['time'] == '13:00') {
                        $doctors_time_arr3[$item->name][4] = $doctors_time_arr2[$item->name][2];
                    } elseif ($doctors_time_arr2[$item->name][3]['time'] == '13:00') {
                        $doctors_time_arr3[$item->name][4] = $doctors_time_arr2[$item->name][3];
                    } elseif ($doctors_time_arr2[$item->name][4]['time'] == '13:00') {
                        $doctors_time_arr3[$item->name][4] = $doctors_time_arr2[$item->name][4];
                    } else {
                        $doctors_time_arr3[$item->name][4] = [
                            'name' => 'noname',
                            'specialization' => 'specialization',
                            'doctor' => 'nodoctor',
                            'time' => '19:00',
                            'status' => '2'
                        ];
                    }

                    // проверяем 6-ку
                    if ($doctors_time_arr2[$item->name][0]['time'] == '14:00') {
                        $doctors_time_arr3[$item->name][5] = $doctors_time_arr2[$item->name][0];
                    } elseif ($doctors_time_arr2[$item->name][1]['time'] == '14:00') {
                        $doctors_time_arr3[$item->name][5] = $doctors_time_arr2[$item->name][1];
                    } elseif ($doctors_time_arr2[$item->name][2]['time'] == '14:00') {
                        $doctors_time_arr3[$item->name][5] = $doctors_time_arr2[$item->name][2];
                    } elseif ($doctors_time_arr2[$item->name][3]['time'] == '14:00') {
                        $doctors_time_arr3[$item->name][5] = $doctors_time_arr2[$item->name][3];
                    } elseif ($doctors_time_arr2[$item->name][4]['time'] == '14:00') {
                        $doctors_time_arr3[$item->name][5] = $doctors_time_arr2[$item->name][4];
                    } elseif ($doctors_time_arr2[$item->name][5]['time'] == '14:00') {
                        $doctors_time_arr3[$item->name][5] = $doctors_time_arr2[$item->name][5];
                    } else {
                        $doctors_time_arr3[$item->name][5] = [
                            'name' => 'noname',
                            'specialization' => 'specialization',
                            'doctor' => 'nodoctor',
                            'time' => '19:00',
                            'status' => '2'
                        ];
                    }
                    // проверяем 7-ку
                    if ($doctors_time_arr2[$item->name][0]['time'] == '15:00') {
                        $doctors_time_arr3[$item->name][6] = $doctors_time_arr2[$item->name][0];
                    } elseif ($doctors_time_arr2[$item->name][1]['time'] == '15:00') {
                        $doctors_time_arr3[$item->name][6] = $doctors_time_arr2[$item->name][1];
                    } elseif ($doctors_time_arr2[$item->name][2]['time'] == '15:00') {
                        $doctors_time_arr3[$item->name][6] = $doctors_time_arr2[$item->name][2];
                    } elseif ($doctors_time_arr2[$item->name][3]['time'] == '15:00') {
                        $doctors_time_arr3[$item->name][6] = $doctors_time_arr2[$item->name][3];
                    } elseif ($doctors_time_arr2[$item->name][4]['time'] == '15:00') {
                        $doctors_time_arr3[$item->name][6] = $doctors_time_arr2[$item->name][4];
                    } elseif ($doctors_time_arr2[$item->name][5]['time'] == '15:00') {
                        $doctors_time_arr3[$item->name][6] = $doctors_time_arr2[$item->name][5];
                    } elseif ($doctors_time_arr2[$item->name][6]['time'] == '15:00') {
                        $doctors_time_arr3[$item->name][6] = $doctors_time_arr2[$item->name][6];
                    } else {
                        $doctors_time_arr3[$item->name][6] = [
                            'name' => 'noname',
                            'specialization' => 'specialization',
                            'doctor' => 'nodoctor',
                            'time' => '19:00',
                            'status' => '2'
                        ];
                    }
                    // проверяем 8-ку
                    if ($doctors_time_arr2[$item->name][0]['time'] == '16:00') {
                        $doctors_time_arr3[$item->name][7] = $doctors_time_arr2[$item->name][0];
                    } elseif ($doctors_time_arr2[$item->name][1]['time'] == '16:00') {
                        $doctors_time_arr3[$item->name][7] = $doctors_time_arr2[$item->name][1];
                    } elseif ($doctors_time_arr2[$item->name][2]['time'] == '16:00') {
                        $doctors_time_arr3[$item->name][7] = $doctors_time_arr2[$item->name][2];
                    } elseif ($doctors_time_arr2[$item->name][3]['time'] == '16:00') {
                        $doctors_time_arr3[$item->name][7] = $doctors_time_arr2[$item->name][3];
                    } elseif ($doctors_time_arr2[$item->name][4]['time'] == '16:00') {
                        $doctors_time_arr3[$item->name][7] = $doctors_time_arr2[$item->name][4];
                    } elseif ($doctors_time_arr2[$item->name][5]['time'] == '16:00') {
                        $doctors_time_arr3[$item->name][7] = $doctors_time_arr2[$item->name][5];
                    } elseif ($doctors_time_arr2[$item->name][6]['time'] == '16:00') {
                        $doctors_time_arr3[$item->name][7] = $doctors_time_arr2[$item->name][6];
                    } elseif ($doctors_time_arr2[$item->name][7]['time'] == '16:00') {
                        $doctors_time_arr3[$item->name][7] = $doctors_time_arr2[$item->name][7];
                    } else {
                        $doctors_time_arr3[$item->name][7] = [
                            'name' => 'noname',
                            'specialization' => 'specialization',
                            'doctor' => 'nodoctor',
                            'time' => '19:00',
                            'status' => '2'
                        ];
                    }
                    // проверяем 9-ку
                    if ($doctors_time_arr2[$item->name][0]['time'] == '17:00') {
                        $doctors_time_arr3[$item->name][8] = $doctors_time_arr2[$item->name][0];
                    } elseif ($doctors_time_arr2[$item->name][1]['time'] == '17:00') {
                        $doctors_time_arr3[$item->name][8] = $doctors_time_arr2[$item->name][1];
                    } elseif ($doctors_time_arr2[$item->name][2]['time'] == '17:00') {
                        $doctors_time_arr3[$item->name][8] = $doctors_time_arr2[$item->name][2];
                    } elseif ($doctors_time_arr2[$item->name][3]['time'] == '17:00') {
                        $doctors_time_arr3[$item->name][8] = $doctors_time_arr2[$item->name][3];
                    } elseif ($doctors_time_arr2[$item->name][4]['time'] == '17:00') {
                        $doctors_time_arr3[$item->name][8] = $doctors_time_arr2[$item->name][4];
                    } elseif ($doctors_time_arr2[$item->name][5]['time'] == '17:00') {
                        $doctors_time_arr3[$item->name][8] = $doctors_time_arr2[$item->name][5];
                    } elseif ($doctors_time_arr2[$item->name][6]['time'] == '17:00') {
                        $doctors_time_arr3[$item->name][8] = $doctors_time_arr2[$item->name][6];
                    } elseif ($doctors_time_arr2[$item->name][7]['time'] == '17:00') {
                        $doctors_time_arr3[$item->name][8] = $doctors_time_arr2[$item->name][7];
                    } elseif ($doctors_time_arr2[$item->name][8]['time'] == '17:00') {
                        $doctors_time_arr3[$item->name][8] = $doctors_time_arr2[$item->name][8];
                    } else {
                        $doctors_time_arr3[$item->name][8] = [
                            'name' => 'noname',
                            'specialization' => 'specialization',
                            'doctor' => 'nodoctor',
                            'time' => '19:00',
                            'status' => '2'
                        ];
                    }
        }
        return view('shedule')->with(['b_doctors'=>$doctors,'b_doctors_time'=>$doctors_time, 'b_doct_time_arr'=>$doctors_time_arr3]);
    }

//    public function chooseDate(Request $request, $date){
    public function chooseDate(Request $request){
dd($request->input('date'));
    }
}

