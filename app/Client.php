<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $timestamps = false; // при создании таблицы Ларавель автоматиески одижает 2 поля: updated_at и created_at. Теперь мы их отключили
}
