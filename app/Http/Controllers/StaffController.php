<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;

class StaffController extends Controller
{
    public function staff() {
/*        $message = 'Hello Stuf';
        $message1 = 'Hello Stuf!!!!!!!';
        return view('staff')->with(['b_message'=>$message, 'b_message1'=>$message1]);*/
        $allDoctors = Doctor::get();
        return view('staff')->with(['b_allDoctors'=>$allDoctors]);
    }
}
