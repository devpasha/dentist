<?php

namespace App\Http\Controllers\Auth;
use App\Client;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/home'; было по умолчанию...
      protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){
        try{
           $this->validate($request, [
               'login'=>'required|string|min:3',
               'password' => 'required|min:6'
           ]);
           // как этот метод понимает из какой таблицы - в трейте один из методов принимает вторым параметром имя таблицы (наверное).
            //Но еще вопросоткуда метод атемпт берется у этого фасада?
           if(Auth::attempt(['login' => $request->input('login'), 'password'=>$request->input('password')])){
               if($request->input('login') != 'admin'){
                   return redirect()->action('User\UserController@showClientOrder', ['name'=>$request->input('login')]);
//                   return redirect()->action('User\UserController@showClientOrder', ['id'=>$id]); // вопрос: хочу что бы в роуте было только id, но надо передать и id и name, можно?!!7!?!
                }
                else {
                    return redirect()->route('adminList');
                }
           }
           else{
               dd("Вы ввели неправильный логин или пароль");
           }

        }catch (ValidationException $e){
            dd($e);
        }
    }

    public function logout(Request $request)
    {
//        $this->guard()->logout(); // надо закомментировать????!!
        $request->session()->invalidate();
        return redirect()->route('pageMain');
    }
}
