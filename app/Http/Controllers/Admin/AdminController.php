<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    // выводим список всех записанных клиентов
    public function showAdminList() {
    $clients = Client::select()->get();
    return view('auth.admin')->with(['b_clients'=>$clients]);
    }

    // обработчик статуса клиента Подтвержден/Отменен
    public function clientStatusChange(Request $request, $id){
//        dd("sdfhsdfjsdf");
/*        $q = $this->showAdminList();
        dd($q);*/
//        $this->showAdminList();

//        $a = $request->input('clientId');
//        $status = (int)$request->input($id);
        $status = $request->input($id);
//        dd($status);
//        echo($status);
//        echo "<br>";
//        dd($_POST);
//        dd($_POST["test"]);
//        dd($_POST["clientId"]);
//        dd($_POST["'client'.$a"]);
//        Client::where('id', $request->input('clientId'))->update(['status'=> $request->input('client'.$a)]);
//        Client::where('id', $id)->update(['status'=> $request->input('client'.$a)]);
        Client::where('id', $id)->update(['status'=> $status]);
//        dd("a1234567");
        return redirect()->route('adminList');
//        redirect('http://dentist/admin');
    }
}
