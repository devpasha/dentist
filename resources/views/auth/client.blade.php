@extends('body.site')
@section('content')
    {{--<h2 style="text-align: left;">Вы зашли как Клиент</h2>--}}
    <div class="foo"><h2 style="text-align: center;">Вы зашли как Клиент</h2></div>
    <div class="foo1"><form action="/admin" method="post">
        {{csrf_field()}}
        <input type="hidden" name="logout" value="logout">
        <input type="submit" value="Выйти" id="logout" class="enroll-doc">
    </form>
    </div>
    <br>
    <br>
{{--    <form class="rf" action="" method="POST" id="logInAdmin">
        {{csrf_field()}}
        <input type='text' name="login" placeholder="Ваш логин" class="rfield" id="user_name" />
        <input type='password' name="password" placeholder="Ваш пароль" class="rfield" id="user_family" />
        <input type='submit'  value="Войти" class="btn_submit disabled">
    </form>--}}
    <section id="client_page">
        <div class="client_table">
            <div id="client">
            <table>
                <tr>
                    <th>
                        Имя
                    </th>
                    <th>
                        Телефон
                    </th>
                    <th>
                        Услуга
                    </th>
                    <th>
                        Доктор
                    </th>
                    <th>
                        Дата
                    </th>
                    <th>
                        Время
                    </th>
                    <th>
                        Комментарий
                    </th>
                    <th>
                        Статус
                    </th>
                </tr>
        {{--!!!!Вопрос: даже если в массие всего один элемент и массив одномерный обратиться к свойству $client->name нельзя? надо только через foreach&?--}}
                @foreach($b_client as $client)
                    <tr>
                        <td>
                            {{$client->name}}
                        </td>
                        <td>
                            {{$client->phone}}
                        </td>
                        <td>
                            {{$client->service}}
                        </td>
                        <td>
                            {{$client->doctor}}
                        </td>
                        <td>
                            {{$client->day}}
                        </td>
                        <td>
                            {{$client->time}}
                        </td>
                        <td>
                            {{$client->comment}}
                        </td>
                        <td>
                           {{$client->status}}
                        </td>
                    </tr>
                @endforeach
            </table>
            </div>
        </div>
    </section>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
@endsection