@extends('body.site')
@section('content')
	<div class="content">
		<section id="contacts_page" class="fixed">
			<div class="contacts_table">
				<div class="contacts-items">
					<div class="contacts-item">
						<img src="img/call.png" alt="">
						<a href="tel:0964157004">+38 (050) 111 11 11</a>
					</div>
					<div class="contacts-item">
						<img src="img/call.png" alt="">
						<a href="tel:0964157004">+38 (050) 111 11 11</a>
					</div>
					<div class="contacts-item">
						<img src="img/call.png" alt="">
						<a href="tel:0964157004">+38 (050) 111 11 11</a>
					</div>
					<div class="contacts-item">
						<img src="img/metro.png" alt="">
						<a href="tel:0964157004">Театральная, ул. Пушкинская, 11А</a>
					</div>
					<div class="contacts-item">
						<img src="img/mail.png" alt="">
						<a href="mailto:htd.lawgroup@gmail.com">stomatology@gmail.com</a>
					</div>
				</div>
				<div id="map" class="mymap"></div>
			</div>
			<div class="request">
				<div class="request-form">
					<form action="{{route('pageMain')}}" method="post">
						{{csrf_field()}}
						<h3>Заказать услугу</h3>
						<input type="text" placeholder="Ваше имя" name="name" required><br>
						<input type="text" placeholder="Номер телефона" name="phone" required><br>
						<input type="time" name="cron" value="09:00" min="09:00" max="17:00"><br>
						<input type="date" name="day" placeholder="Выберите дату"><br>
						<input type="text" id="serv" name="service" onkeyup="myFunction()" placeholder="Вид услуги" >
						<ul id="serv-select">
							<li><span class="services-choice">Общие виды работ</span></li>
							<li><span class="services-choice">Консультация</span></li>
							<li><span class="services-choice">Оказание скорой помощи</span></li>
							<li><span class="services-choice">Рентген</span></li>
						</ul>
						<input type="text" name="doctor" id="doc-choice" onkeyup="myFunctionDocs()" placeholder="Выберите врача" >
						<ul id="doc-select">
							<li><span class="doc-choices">Василий Петрович</span></li>
							<li><span class="doc-choices">Александр Юриевич</span></li>
							<li><span class="doc-choices">Галина Семеновна</span></li>
							<li><span class="doc-choices">Валентина Сергеевна</span></li>
						</ul>
						<ul>
							<textarea name="comment"></textarea>
						</ul>
						<input type="submit" value="Записаться" class="enroll-doc">
					</form>
				</div>
			</div>
		</section>
		<div id="page">contacts</div>
	</div>
</div>
@endsection