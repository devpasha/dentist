@extends('body.site')
@section('content')
	<div class="content">
		<section id="price_page" class="fixed">
			<div class="price_table">
				<table id="price">
					<tr>
						<th>Виды работ</th>
						<th>Цена, грн</th>
					</tr>
					<tr>
						<td>Общие виды работ</td>
						<td></td>
					</tr>
					<tr>
						<td>Консультация</td>
						<td>44</td>
					</tr>
					<tr>
						<td>Оказание скорой помощи</td>
						<td>203</td>
					</tr>
					<tr>
						<td>Рентген</td>
						<td>80</td>
					</tr>
					<tr>
						<td>Анестезия проводниковая</td>
						<td>80</td>
					</tr>
					<tr>
						<td>Анестезия аппликационная, инъекция</td>
						<td>51</td>
					</tr>
					<tr>
						<td>Профилактический приём</td>
						<td></td>
					</tr>
					<tr>
						<td>Лечение гиперестезии фторсодержащими препаратами</td>
						<td>44</td>
					</tr>
					<tr>
						<td>Отбеливание зубов</td>
						<td>2248</td>
					</tr>
					<tr>
						<td>Отбеливание внутрикоренное </td>
						<td>377</td>
					</tr>
					<tr>
						<td>Общие виды работ</td>
						<td></td>
					</tr>
					<tr>
						<td>Консультация</td>
						<td>44</td>
					</tr>
					<tr>
						<td>Оказание скорой помощи</td>
						<td>203</td>
					</tr>
					<tr>
						<td>Рентген</td>
						<td>80</td>
					</tr>
					<tr>
						<td>Анестезия проводниковая</td>
						<td>80</td>
					</tr>
					<tr>
						<td>Анестезия аппликационная, инъекция</td>
						<td>51</td>
					</tr>
					<tr>
						<td>Профилактический приём</td>
						<td></td>
					</tr>
					<tr>
						<td>Лечение гиперестезии фторсодержащими препаратами</td>
						<td>44</td>
					</tr>
					<tr>
						<td>Отбеливание зубов</td>
						<td>2248</td>
					</tr>
					<tr>
						<td>Отбеливание внутрикоренное </td>
						<td>377</td>
					</tr>
				</table>
			</div>
			<div class="request">
				<div class="request-form">
					<form action="{{route('pageMain')}}" method="post">
						{{csrf_field()}}
						<h3>Заказать услугу</h3>
						<input type="text" placeholder="Ваше имя" name="name" required><br>
						<input type="text" placeholder="Номер телефона" name="phone" required><br>
						<input type="time" name="cron" value="09:00" min="09:00" max="17:00"><br>
						<input type="date" name="day" placeholder="Выберите дату"><br>
						<input type="text" id="serv" name="service" onkeyup="myFunction()" placeholder="Вид услуги" >
						<ul id="serv-select">
							<li><span class="services-choice">Общие виды работ</span></li>
							<li><span class="services-choice">Консультация</span></li>
							<li><span class="services-choice">Оказание скорой помощи</span></li>
							<li><span class="services-choice">Рентген</span></li>
						</ul>
						<input type="text" name="doctor" id="doc-choice" onkeyup="myFunctionDocs()" placeholder="Выберите врача" >
						<ul id="doc-select">
							<li><span class="doc-choices">Василий Петрович</span></li>
							<li><span class="doc-choices">Александр Юриевич</span></li>
							<li><span class="doc-choices">Галина Семеновна</span></li>
							<li><span class="doc-choices">Валентина Сергеевна</span></li>
							<li><span class="doc-choices">Мария Анатолиевна</span></li>
							<li><span class="doc-choices">Виктория Степановна</span></li>
						</ul>
						<ul>
							<textarea name="comment"></textarea>
						</ul>
						<input type="submit" value="Записаться" class="enroll-doc">
					</form>
				</div>
			</div>
		</section>
		<div id="page">price</div>
	</div>
	</div>
@endsection