@extends('body.site')
@section('content')
	<div class="content">
		<section id="serv-content">
			<div class="fixed">
				<div class="serv-left">
					<div class="serv-menu">
						<h1>ТЕРАПЕВТИЧЕСКАЯ СТОМАТОЛОГИЯ</h1>
						<ul>
							<li><a href="">Чистка зубов</a></li>
							<li><a href="">Лечение кариеса</a></li>
							<li><a href="">Отбеливание зубов</a></li>
							<li><a href="">Реставрация зубов</a></li>
							<li><a href="">Лечение корневых каналов</a></li>
							<li><a href="">Лечение пародонтита</a></li>
						</ul>
						<h1>ХИРУРГИЧЕСКАЯ СТОМАТОЛОГИЯ</h1>
						<ul>
							<li><a href="">Удаление зубов</a></li>
							<li><a href="">Имплантация</a></li>
						</ul>
						<h1>ПРОТЕЗИРОВАНИЕ ЗУБОВ</h1>
						<ul>
							<li><a href="">Виниры</a></li>
							<li><a href="">Люминиры</a></li>
							<li><a href="">Коронки металлокерамические</a></li>
							<li><a href="">Коронки безметалловые</a></li>
							<li><a href="">Съемные протезы бюгельные</a></li>
							<li><a href="">Нейлоновые протезы</a></li>
							<li><a href="">Пластмассовые протезы</a></li>
						</ul>
						<h1>ОРТОДОНТИЯ</h1>
						<ul>
							<li><a href="">Брекеты</a></li>
							<li><a href="">Каппы</a></li>
							<li><a href="">Выравнивание зубов</a></li>
						</ul>
					</div>
				</div>
				<div class="serv-right">
					<div class="reasons">
						<div class="title">Причины возникновения желтизны эмали</div>
						<div class="reason">
							<img src="{{asset('img/pentagon.png')}}" alt="">
							<p>курение</p>
						</div>
						<div class="reason">
							<img src="{{asset('img/pentagon.png')}}" alt="">
							<p>возраст и наследствен- ность</p>
						</div>
						<div class="reason">
							<img src="{{asset('img/pentagon.png')}}" alt="">
							<p>употребление воды с высоким содержанием фтора</p>
						</div>
						<div class="reason">
							<img src="{{asset('img/pentagon.png')}}" alt="">
							<p>плохая гигиена зубов</p>
						</div>
						<div class="reason">
							<img src="{{asset('img/pentagon.png')}}" alt="">
							<p>частое употребление кофе и черного чая</p>
						</div>
					</div>
					<div class="service-description">
						<h1>ОТБЕЛИВАНИЕ ЗУБОВ</h1>
						<p class="description">
							В течение всей жизни естественный цвет зубной эмали может портиться вследствие воздействия самых разных факторов. Негативное воздействие на белизну улыбки оказывает курение, а также употребление в пищу продуктов с красителями. Также нездоровый цвет зубам могут придать хронические заболевания ротовой полости и наличие тетрациклиновых зубов. Так как любой человек обеспокоен своим внешним видом, а красивая улыбка – это одна из основополагающих деталей привлекательности, то лучшим способом изменить некрасивый цвет эмали будет отбеливание зубов.</p>
						<button class="accordion">Механическое отбеливание</button>
						<div class="service-text">
							<p>В зависимости от того, насколько поврежден зуб, стоматолог выбирает степень воздействия на него посредством лазера. Этот
								метод гарантирует белоснежную улыбку всего лишь после часового сеанса. В зависимости от профессионализма
								стоматолога и качества используемых материалов, стоимость на такую услугу может варьироваться.Фотоотбеливание
								производится с помощью галогенового света. Особое внимание нужно обратить на то, что этот метод идеально подойдет
								пациентам, чувствительность зубов которого очень высокая.</p>
						</div>

						<button class="accordion">Химическое отбеливание</button>
						<div class="service-text">
							<p>В зависимости от того, насколько поврежден зуб, стоматолог выбирает степень воздействия на него посредством лазера. Этот
								метод гарантирует белоснежную улыбку всего лишь после часового сеанса. В зависимости от профессионализма
								стоматолога и качества используемых материалов, стоимость на такую услугу может варьироваться.Фотоотбеливание
								производится с помощью галогенового света. Особое внимание нужно обратить на то, что этот метод идеально подойдет
								пациентам, чувствительность зубов которого очень высокая.</p>
						</div>

						<button class="accordion">Лазерное отбеливание</button>
						<div class="service-text">
							<p>В зависимости от того, насколько поврежден зуб, стоматолог выбирает степень воздействия на него посредством лазера. Этот
								метод гарантирует белоснежную улыбку всего лишь после часового сеанса. В зависимости от профессионализма
								стоматолога и качества используемых материалов, стоимость на такую услугу может варьироваться.Фотоотбеливание
								производится с помощью галогенового света. Особое внимание нужно обратить на то, что этот метод идеально подойдет
								пациентам, чувствительность зубов которого очень высокая.</p>
						</div>
						<div id="enroll-serv">Записаться</div>
						<div id="watch-price">
							<a href="{{route('pagePrices')}}">Узнать цену</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="feedback">
			<div class="fixed">
				<div class="feedback-item">
					<p class="feedback-text">Очень доволена. <br>Сделали качественно.<br>Рекомендую</p>
					<div class="feedback-person">
						<p>Светлана Петровна, 20 лет</p>
						<img src="{{asset('img/rewier.jpg')}}" alt="">
					</div>
				</div>
				<div class="feedback-item">
					<p class="feedback-text">Очень доволена. <br>Сделали качественно.<br>Рекомендую</p>
					<div class="feedback-person">
						<p>Егениея Ивановна, 25 лет</p>
						<img src="{{asset('img/rewier.jpg')}}" alt="">
					</div>
				</div>
				<div class="feedback-item">
					<p class="feedback-text">Очень доволена. <br>Сделали качественно.<br>Рекомендую</p>
					<div class="feedback-person">
						<p>Мария Леонидовна, 30 лет</p>
						<img src="{{asset('img/rewier.jpg')}}" alt="">
					</div>
				</div>
			</div>
		</section>
		<div id="orderModal">
			<div id="modal-content">
				<div class="request-form">
					<form action="{{route('pageMain')}}" method="post">
						{{csrf_field()}}
						<h3>Заказать услугу</h3>
						<input type="text" placeholder="Ваше имя" name="name" required><br>
						<input type="text" placeholder="Номер телефона" name="phone" required><br>
						<input type="time" name="cron" value="09:00" min="09:00" max="17:00"><br>
						<input type="date" name="day" placeholder="Выберите дату"><br>
						<input type="text" id="serv" name="service" onkeyup="myFunction()" placeholder="Вид услуги" >
						<ul id="serv-select">
							<li><span class="services-choice">Общие виды работ</span></li>
							<li><span class="services-choice">Консультация</span></li>
							<li><span class="services-choice">Оказание скорой помощи</span></li>
							<li><span class="services-choice">Рентген</span></li>
						</ul>
						<input type="text" name="doctor" id="doc-choice" onkeyup="myFunctionDocs()" placeholder="Выберите врача" >
						<ul id="doc-select">
							<li><span class="doc-choices">Василий Петрович</span></li>
							<li><span class="doc-choices">Александр Юриевич</span></li>
							<li><span class="doc-choices">Галина Семеновна</span></li>
							<li><span class="doc-choices">Валентина Сергеевна</span></li>
							<li><span class="doc-choices">Мария Анатолиевна</span></li>
							<li><span class="doc-choices">Виктория Степановна</span></li>
						</ul>
						<ul>
							<textarea name="comment"></textarea>
						</ul>
						<input type="submit" value="Записаться" class="enroll-doc">
					</form>
				</div>
			</div>
		</div>
		<div id="page">services</div>
	</div>
</div>
@endsection