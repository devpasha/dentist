<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        return false; // default
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3',
            'phone'=>'required|string|min:9|max:14',
            'service'=>'required',
            'doctor'=>'required',
            'day'=>'required',
        ];
    }
}


/*    =>  $request->input('name'),
                                    'phone'     =>  $request->input('phone'),
                                    'service'   =>  $request->input('service'),
                                    'doctor'    =>  $request->input('doctor'),
                                    'day'       =>  $request->input('day'),
                                    'time'      =>  $request->input('cron'),
                                    'comment'   =>  $request->input('comment'),)*/