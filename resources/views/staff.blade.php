@extends('body.site')
@section('content')
	<div class="content docs-page">
		<section id="docs">
			<div class="fixed">
					<div class="doctors">
						@foreach($b_allDoctors as $doctor)
							<div class="doctor">
								<img src="{{$doctor->foto}}" alt="">
								<div class="doc-content">
									<div class="doc">
										<h5>{{$doctor->name}}</h5>
										<h6>{{$doctor->specialization}}</h6>
									</div>
									<div class="about-doc">
										<p>Опыт работы - 5 лет</p>
										<p>Сертификаты о прохождении курсов:</p>
										<p>«Стратегия лечения патологии окклюзии по ІІ
											классу. Оптимазация роста челюстных костей»
											— <span>Интердент</span><br>
											«Приминение ортодонтических имплантов» —
											<span>Интердент</span><br>
											«Методы дистилизации моляров» — <span>Интердент</span><br>
											«Технология термоформирования. Приминение в
											общей стоматологии и ортодонтии. Широкие
											возможности выравнивания зубов и ретенции
											невидимыми каппами» — <span>Интердент</span><br>
											«Диагностика и планирование лечения в
											ортодонтии» — <span>Интердент</span><br>
											«Съемная и несъемная ортодонтия. Базовый курс»
											— <span>Интердент</span><br>
											«Ортодонтические проволочные дуги и
											вертикальный паз» — <span>Интердент</span><br>
											«Конструктивные особенности аппарата прямой
											дуги» — <span>Dentaurum</span>, г. Киев, 2003 г.</p>
										<div class="enroll-doc">Записаться</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
			</div>
		</section>
		<section id="schedule">
			<div id="schedule-button"><a href="{{route('pageShedule')}}">Посмотреть расписание врачей</a></div>
		</section>
		<div id="orderModal">
			<div id="modal-content">
				<div class="request-form">
					<form action="{{route('pageMain')}}" method="post">
					{{--<form action="{{route('A')}}" method="post">--}}
						{{csrf_field()}}
						<h3>Заказать услугу</h3>
						<input type="text" placeholder="Ваше имя" name="name" required><br>
						<input type="text" placeholder="Номер телефона" name="phone" required><br>
						<input type="time" name="cron" value="09:00" min="09:00" max="17:00"><br>
						<input type="date" name="day" placeholder="Выберите дату"><br>
						<input type="text" id="serv" name="service" onkeyup="myFunction()" placeholder="Вид услуги" >
						<ul id="serv-select">
							<li><span class="services-choice">Общие виды работ</span></li>
							<li><span class="services-choice">Консультация</span></li>
							<li><span class="services-choice">Оказание скорой помощи</span></li>
							<li><span class="services-choice">Рентген</span></li>
						</ul>
						<input type="text" name="doctor" id="doc-choice" onkeyup="myFunctionDocs()" placeholder="Выберите врача" >
						<ul id="doc-select">
							<li><span class="doc-choices">Василий Петрович</span></li>
							<li><span class="doc-choices">Александр Юриевич</span></li>
							<li><span class="doc-choices">Галина Семеновна</span></li>
							<li><span class="doc-choices">Валентина Сергеевна</span></li>
							<li><span class="doc-choices">Мария Анатолиевна</span></li>
							<li><span class="doc-choices">Виктория Степановна</span></li>
						</ul>
						<ul>
							<textarea name="comment"></textarea>
						</ul>
						<input type="submit" value="Записаться" class="enroll-doc">
					</form>
				</div>
			</div>
		</div>
		<div id="page">staff</div>
		</div>
	</div>
@endsection
