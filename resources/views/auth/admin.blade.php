@extends('body.site')
@section('content')
    <div class="foo"><h2 style="text-align: left;">Вы зашли как Администратор</h2></div>
    {{--<h2 style="text-align: left;">Вы зашли как Администратор</h2><br>--}}
        {{-- как обратиться к роуту, котрый в папке Аuth например route('logout') ???!!!!!!!!--}}

    <div class="foo1"><form action="/admin" method="post"></div>
        {{csrf_field()}}
        <input type="hidden" name="logout" value="logout">
        <input type="submit" value="Выйти" id="logout" class="enroll-doc">
    </form>
{{--    <form class="rf" action="" method="POST" id="logInAdmin">
        {{csrf_field()}}
        <input type='text' name="login" placeholder="Ваш логин" class="rfield" id="user_name" />
        <input type='password' name="password" placeholder="Ваш пароль" class="rfield" id="user_family" />
        <input type='submit'  value="Войти" class="btn_submit disabled">
    </form>--}}
    <div style="text-align: center"> Наши клиенты:</div>
    <br>
    <br>
    {{--<section id="admin_page" class="fixed">--}}
    <section id="admin_page">
        <div class="admin_table">
            <div id="admin">
            <table>
            <tr>
                <th>
                    №
                </th>
                <th>
                    Имя
                </th>
                <th>
                    Телефон
                </th>
                <th>
                    Услуга
                </th>
                <th>
                    Доктор
                </th>
                <th>
                    Дата
                </th>
                <th>
                    Время
                </th>
                <th>
                    Комментарий
                </th>
                <th>
                    Статус
                </th>
            </tr>
            @foreach($b_clients as $client)
            <tr>
                <td>
                {{$loop->index + 1}}
                </td>
                <td>
                    {{$client->name}}
                </td>
                <td>
                    {{$client->phone}}
                </td>
                <td>
                    {{$client->service}}
                </td>
                <td>
                    {{$client->doctor}}
                </td>
                <td>
                    {{$client->day}}
                </td>
                <td>
                    {{$client->time}}
                </td>
                <td>
                    {{$client->comment}}
                </td>
                <td>
                    <form method="post" action="/admin/{{$client->id}}">
                    {{csrf_field()}}
                    @if($client->status)
                    <input type="radio" name="{{$client->id}}" value="1" checked>Подтвержден
                    <input type="radio" name="{{$client->id}}" value="0">Отменен
                    @else
                    <input type="radio" name="{{$client->id}}" value="1">Подтвержден
                    <input type="radio" name="{{$client->id}}" value="0" checked>Отменен
                    @endif
                    {{--Для изменения данных используем метод PUT--}}
                    <input type="hidden" name="_method" value="PUT">
                    <input type="submit" value="Редактировать" class="enroll-doc">
                    </form>
                </td>
            </tr>
            @endforeach
            </table>
                </div>
            </div>
    </section>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
@endsection