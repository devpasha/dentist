<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

// можем указывать вид, как аргумент функции - просто отобразим шаблон. А можем указать после параметра запроса контроллер и метод, который его обработает
/*Route::get('/', function () {
    return view('main');
});*/
// Главная страница
Route::get('/','MainController@main')->name('pageMain');

// Страница услуг
Route::get('servises','ServisesController@servises')->name('pageServises');

// Страница персонала
Route::get('staff','StaffController@staff')->name('pageStaff');

// Страница цен
Route::get('prices','PricesController@prices')->name('pagePrices');

// Страница контактов
Route::get('contacts','ContactsController@contacts')->name('pageContacts');

// Страница контактов
Route::get('shedule','SheduleController@shedule')->name('pageShedule');
//Route::post('shedule/{date}','SheduleController@chooseDate');
Route::post('shedule','SheduleController@chooseDate');

// Обработка формы заявки
Route::post('/','MainController@form')->name('formHadler');
//Route::post('/','MainController@form1');

// Админка
//Route::get('admin', 'Admin\AdminController@admin')->name('pageAdmin');
/*Route::get('admin', function (){
    return view('auth/login');
});*/

// !!!!!!!!!!!В этих роутах поменять guest на client, auth на admin - и проверить, будут ли работать  !!!!!!!!!!!
Route::group(['middleware'=>'guest'], function() {
    // потом подумать, как регестрироваться именно через вызов вьюхи с формой. Какой там должен быть экшн??? А пока пишу как в уроке - черех метод трейта
/*    Route::get('/registration', function () {
        return view('auth/registration');
    });*/
    Route::get('/registration', 'Auth\RegisterController@showRegistrationForm')->name('registration');
    Route::post('/registration', 'Auth\RegisterController@register');

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
//    Route::post('/login', 'User\UserController@showClientOrder')->name('userList'); // было до того, как решил сдлать страницу .../client/3
});
Route::group(['middleware'=>'auth'], function() {
//      Route::get('/client', '')->name('clientList');
/*    Route::get('/client', function () {
        return view('auth/client');
    });*/
    // этот роут работает, но хочу сделать через метод, что бы отображать результаты из базы
/*    Route::get('/admin', function () {
        return view('auth/admin');
    });*/
    Route::get('/admin', 'Admin\AdminController@showAdminList')->name('adminList');
    Route::put('/admin/{id}', 'Admin\AdminController@clientStatusChange')->where(['id'=>'[0-9]+'])->name('clientStatusChange');
    Route::post('/admin', 'Auth\LoginController@logout')->name('logout');

    Route::get('/client/{name}', 'User\UserController@showClientOrder')->where(['name'=>'[a-zA-Zа-яА-я]+'])->name('userList');
});


