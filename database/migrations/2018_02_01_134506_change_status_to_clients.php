<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatusToClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // !!!Разобраться с механизмом миграций!!! : мы не указываем название миграции, потому что Ларавел автоматически выполняет всегда самую крайнюю миграцию? - вроде да!
    public function up()
    {
        Schema::table('clients', function($table) {
            $table->integer('status')->default('0')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
