@extends('body.site')
@section('content')
			<div class="content">
				<section id="mainscreen">
					<div class="fixed">
						<img src="{{asset('img/main.jpg')}}" alt="">
						<div class="not-menu">
							<div class="not-menu-item"><a href="{{route('pageStaff')}}">Лучшие<br> специалисты</a></div>
							<div class="not-menu-item"><a href="{{route('pageShedule')}}">Расписание<br> врачей</a></div>
							<div class="not-menu-item"><a href="{{route('pageServises')}}">Перечень<br> услуг</a></div>
							<div class="not-menu-item"><a href="{{route('pagePrices')}}">Стоимость<br> услуг</a></div>
						</div>
					</div>
				</section>
				<section id="welcome">
					<div class="welcome-left">
						<h1>ДОБРО ПОЖАЛОВАТЬ В<br><span>СТОМАТОЛОГИЧЕСКУЮ КЛИНИКУ</span>!</h1>
						<p>Nulla facilisi. Sed eu sapien non
		felis auctor sollicitudinporttitor eu orci.
		Aliquam erat volutpat. Quisque finibus
		posuere mi id pretium. Pellentesque sit
		amet mauris orci. Nulla sed est vitae justo
		commodotristique eget sit amet neque.
		Mauris a magna tristiquelacus maximus
		iaculis id ut nisl. Ut maximus nulla dolor,
		non tinciduntsapien hendrerit vel. Fusce ac
		nuncfaucibus, molestie massa at, blandit
		lorem. Aeneanarcu tortor, laoreet at
		maximus sed, sollicitudin et ex.</p>
					</div>
					<div class="welcome-right">
						<div id="grafic">
							<h2><i class="fa fa-clock-o" aria-hidden="true"></i>График работы</h2>
							<table>
								<tr>
									<td>Понедельник</td>
									<td>09:00 - 18:00</td>
								</tr>
								<tr>
									<td>Вторник</td>
									<td>09:00 - 18:00</td>
								</tr>
								<tr>
									<td>Среда</td>
									<td>09:00 - 18:00</td>
								</tr>
								<tr>
									<td>Четверг</td>
									<td>09:00 - 18:00</td>
								</tr>
								<tr>
									<td>Пятница</td>
									<td>09:00 - 18:00</td>
								</tr>
								<tr>
									<td>Суббота</td>
									<td>09:00 - 18:00</td>
								</tr>
								<tr>
									<td>Воскресенье</td>
									<td>Выходной</td>
								</tr>
							</table>
							<div id="enroll">Записаться</div>
						</div>
					</div>
				</section>
				<section id="services">
					<div class="fixed">
						<h1>УСЛУГИ</h1>
						<div class="serv-wrap">
							<div class="serv">
								<h2>Терапевтическая стоматология</h2>
								<ul>
									<li>Чистка зубов</li>
									<li>Лечение кариеса</li>
									<li>Отбеливание зубов</li>
									<li>Реставрация зубов</li>
									<li>Лечение корневых каналов</li>
									<li>Лечение пародонтита</li>
								</ul>
							</div>
							<div class="serv">
								<h2>Хирургическая стоматология</h2>
								<ul>
									<li>Удаление зубов</li>
									<li>Имплантация</li>
								</ul>
							</div>
							<div class="serv">
								<h2>Протезирование зубов</h2>
								<ul>
									<li>Виниры</li>
									<li>Люминиры</li>
									<li>Коронки металлокерамические</li>
									<li>Коронки безметалловые</li>
									<li>Съемные протезы бюгельные</li>
									<li>Нейроновые протезы</li>
									<li>Пластмассовые протезы</li>
								</ul>
							</div>
							<div class="serv">
								<h2>Ортодонтия</h2>
								<ul>
									<li>Чистка зубов</li>
									<li>Лечение кариеса</li>
									<li>Отбеливание зубов</li>
									<li>Реставрация зубов</li>
									<li>Лечение корневых каналов</li>
									<li>Лечение пародонтита</li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<section id="docs">
					<div class="fixed">
						<h1>НАШИ ДОКТОРА</h1>
						<div class="doctors">
							@foreach($b_doctors as $doctor)
								<div class="doctor">
									<img src="{{$doctor->foto}}" alt="">
									<div class="doc-name">
										<h5>{{$doctor->name}}</h5>
										<h6>{{$doctor->specialization}}</h6>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</section>
				<section id="feedback">
					<div class="fixed">
						<div class="feedback-item">
							<p class="feedback-text">Очень доволена. <br>Сделали качественно.<br>Рекомендую</p>
							<div class="feedback-person">
								<p>Светлана Петровна, 20 лет</p>
								<img src="{{asset('img/rewier.jpg')}}" alt="">
							</div>
						</div>
						<div class="feedback-item">
							<p class="feedback-text">Очень доволена. <br>Сделали качественно.<br>Рекомендую</p>
							<div class="feedback-person">
								<p>Егениея Ивановна, 25 лет</p>
								<img src="{{asset('img/rewier.jpg')}}" alt="">
							</div>
						</div>
						<div class="feedback-item">
							<p class="feedback-text">Очень доволена. <br>Сделали качественно.<br>Рекомендую</p>
							<div class="feedback-person">
								<p>Мария Леонидовна, 30 лет</p>
								<img src="{{asset('img/rewier.jpg')}}" alt="">
							</div>
						</div>
					</div>
				</section>
				<div id="page">main</div>
			</div>
		</div>
	<div id="orderModal">
		<div id="modal-content">
			<div class="request-form">
				<form action="{{route('pageMain')}}" method="post">
					{{csrf_field()}}
					<h3>Заказать услугу</h3>
					{{--<input type="text" placeholder="Ваше имя" name="name" required><br>--}}
					<input type="text" placeholder="Ваше имя" name="name"><br>
					{{--<input type="text" placeholder="Номер телефона" name="phone" required><br>--}}
					<input type="text" placeholder="Номер телефона" name="phone"><br>
					<input type="time" name="cron" value="09:00" min="09:00" max="17:00"><br>
					<input type="date" name="day" placeholder="Выберите дату"><br>
					<input type="text" id="serv" name="service" onkeyup="myFunction()" placeholder="Вид услуги" >
					<ul id="serv-select">
						<li><span class="services-choice">Общие виды работ</span></li>
						<li><span class="services-choice">Консультация</span></li>
						<li><span class="services-choice">Оказание скорой помощи</span></li>
						<li><span class="services-choice">Рентген</span></li>
					</ul>
					<input type="text" name="doctor" id="doc-choice" onkeyup="myFunctionDocs()" placeholder="Выберите врача" >
					<ul id="doc-select">
						<li><span class="doc-choices">Василий Петрович</span></li>
						<li><span class="doc-choices">Александр Юриевич</span></li>
						<li><span class="doc-choices">Галина Семеновна</span></li>
						<li><span class="doc-choices">Валентина Сергеевна</span></li>
						<li><span class="doc-choices">Мария Анатолиевна</span></li>
						<li><span class="doc-choices">Виктория Степановна</span></li>
					</ul>
					<ul>
						<textarea name="comment"></textarea>
					</ul>
					<input type="submit" value="Записаться" class="enroll-doc">
				</form>
			</div>
		</div>
	</div>
@endsection

